return {
  {
    "williamboman/mason.nvim",
    config = function()
      require("mason").setup()
    end,
  },
  {
    "williamboman/mason-lspconfig.nvim",
    config = function()
      require("mason-lspconfig").setup({
        ensure_installed = {
          "angularls",
          "clangd",
          "omnisharp",
          "cmake",
          "cssls",
          "dockerls",
          "eslint",
          "emmet_ls",
          "gopls",
          "gradle_ls",
          "graphql",
          "groovyls",
          "html",
          "helm_ls",
          "jsonls",
          "jdtls",
          "tsserver",
          "lua_ls",
          "marksman",
          "intelephense",
          "pyright",
          "rust_analyzer",
          "sqlls",
          "taplo",
          "tailwindcss",
          "yamlls",
        },
      })
    end,
  },
  {
    "neovim/nvim-lspconfig",
    config = function()
      local lsp = require("lspconfig")
      lsp.angularls.setup({})
      lsp.clangd.setup({})
      lsp.omnisharp.setup({})
      lsp.cmake.setup({})
      lsp.cssls.setup({})
      lsp.dockerls.setup({})
      lsp.eslint.setup({})
      lsp.emmet_ls.setup({})
      lsp.gopls.setup({})
      lsp.gradle_ls.setup({})
      lsp.graphql.setup({})
      lsp.groovyls.setup({})
      lsp.html.setup({})
      lsp.helm_ls.setup({})
      lsp.jsonls.setup({})
      lsp.jdtls.setup({})
      lsp.tsserver.setup({})
      lsp.lua_ls.setup({})
      lsp.marksman.setup({})
      lsp.intelephense.setup({})
      lsp.pyright.setup({})
      lsp.rust_analyzer.setup({})
      lsp.sqlls.setup({})
      lsp.taplo.setup({})
      lsp.tailwindcss.setup({})
      lsp.yamlls.setup({})
    end,
  },
  {
    "nvim-cmp",
    dependencies = { "hrsh7th/cmp-emoji" },
    opts = function(_, opts)
      table.insert(opts.sources, { name = "emoji" })
    end,
  },
}
