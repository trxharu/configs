return {
  {
    "rebelot/kanagawa.nvim",
    config = function()
      require("kanagawa").setup({
        compile = false,
        undercurl = true,
        functionStyle = {},
        commentStyle = { italic = true },
        keywordStyle = { italic = true },
        statementStyle = { bold = true },
        transparent = true,
        dimInactive = false,
        terminalColors = true,
      })
      vim.cmd("colorscheme kanagawa-dragon")
    end,
  },
}
