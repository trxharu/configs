#! /bin/bash

# update keys
key="hmac-sha256:ksgdev.net.:your_tsig_key"
hostname="$(hostname)"
machine_ip="$(hostname -I | tr -d '[:space:]')"

ttl=3600
ns_ip="$(nslookup -querytype=A $hostname | grep 'Address:' | awk 'NR == 2 { print $2 }' | tr -d '[:space:]')"

if [[ "$host_ip" == "" ]]; then
        echo "Hostname DNS not updated."
        echo "Updating DNS record..."
        (echo "update add $hostname $ttl A $machine_ip"; echo "send") | nsupdate -v -y $key
        echo "DNS records updated."
elif [[ "$machine_ip" != "$ns_ip" ]]; then
        echo "DNS is updated for old IPv4. New: $machine_ip Old: $host_ip"
        echo "Deleting old DNS record..."
        (echo "update delete $hostname A"; echo "update add $hostname $ttl A $machine_ip"; echo "send") | nsupdate -v -y $key
        echo "New hostname DNS updated."
else
        echo "DNS already updated."
fi