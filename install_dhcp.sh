#!/bin/bash

dnf update
dnf install -y kea
cp -f kea-dhcp4.conf /etc/kea/
systemctl enable --now kea-dhcp4